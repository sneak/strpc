#!/usr/bin/env python3
#234567891123456789212345678931234567894123456789512345678961234567897123456789
# encoding: utf-8

from datetime import datetime, tzinfo, timedelta
from sanelogging import log
from time import sleep
import json
import os
import pathlib
import socket
import subprocess
import uuid

def dirs_in_dir(path):
    assert os.path.isdir(path)
    items = os.listdir(path)
    output = []
    for fn in items:
        if os.path.isdir(path + '/' + fn):
            output.append(path + '/' + fn)
    return output

class simple_utc(tzinfo):
    def tzname(self,**kwargs):
        return "UTC"
    def utcoffset(self, dt):
        return timedelta(0)

def isodatetime():
    return str(
        datetime.utcnow().replace(tzinfo=simple_utc()).isoformat()
    ).replace('+00:00', 'Z')

def read_file(path):
    with open(path) as f:
        data = f.read()
    return data

# FIXME is there some auto reading-and-writing JSONFile class out there
def read_json_file(path):
    return json.loads(read_file(path))

def json_pretty(obj):
    return json.dumps(obj, sort_keys=True, indent=4, separators=(',', ': '))

def write_json_file(path, obj):
    write_file(path, json_pretty(obj))

def write_file(path,content):
    with open(path, 'w') as file:
        file.write(content)
