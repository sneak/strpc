default: devserver

build:
	docker build -t sneak/strpc .

devserver:
	/bin/bash bin/devserver
