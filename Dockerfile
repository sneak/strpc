FROM phusion/baseimage:0.11

ENV LOG_TO_SYSLOG 1

ENV APPDIR /var/app

VOLUME /strpc

ENV STRPC_BASE /var/lib/strpc

ADD ./sources.list /etc/apt/sources.list

RUN apt update && apt install -y python3-pip docker.io

RUN pip3 install pipenv

RUN mkdir -p /var/lib/strpc

ADD . /var/app

WORKDIR /var/app

RUN pipenv install

RUN mkdir -p /etc/service/strpcd && \
	mv /var/app/strpcd.run /etc/service/strpcd/run && \
	chmod +x /etc/service/strpcd/run

RUN apt -y autoremove && \
	apt-get clean && \
	rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
